#!/usr/bin/python3

import smtplib
# # 发送字符串的邮件
from email.mime.text import MIMEText
import requests
import json
# 处理多种形态的邮件主体我们需要 MIMEMultipart 类
# from email.mime.multipart import MIMEMultipart
# 处理图片需要 MIMEImage 类
# from email.mime.image import MIMEImage
# 设置服务器所需信息
fromaddr = 'wangshiru2020@126.com'
# 邮件发送方邮箱地址
password = 'JUBMSUDZGUVSHKYF'
# 密码(部分邮箱为授权码)
# 邮件接受方邮箱地址，注意需要[]包裹，这意味着你可以写多个邮件地址群发


# 可写函数说明
def getWeather(params):
    lt = []
    meg = ''
    url = 'https://devapi.qweather.com/v7/weather/now'
    for k, v in params.items():
        lt.append(k+'='+str(v))
    # 这个是在for循环外面的，就是将列表的元素之间用&符号连接起来
    query_string = '&'.join(lt)
    url = url + '?'+query_string
    print(url)
    city_weather = requests.get(url)
    result = json.loads(city_weather.text)
    print(result)
    if result:
        meg = '当前天气情况：'+result['now']['text']+'\n当前温度：'+result['now']['temp']+'摄氏度\n实况风向：'+result['now']['windDir']+result['now']['windScale'] + '级'
    if float(result['now']['temp']) > 30.00:
        meg += '\n今天天气温度较高，建议穿半袖T恤，夏天紫外线强烈，注意物理和化学防晒结合！\n'
    elif float(result['now']['temp']) < 30.00 and float(result['now']['temp']) > 25.00:
        meg += '\n今天天气温度适宜，可穿半袖T恤，注意防晒哦！\n'
    elif float(result['now']['temp']) < 25.00 and float(result['now']['temp']) > 20.00:
        meg += '\n今天天气温度适宜，可穿长袖T恤、衬衫等衣物，注意防晒哦！\n'
    elif float(result['now']['temp']) < 20.00 and float(result['now']['temp']) > 10.00:
        meg += '\n今天天气凉爽，早晚温差大，注意适当添加衣物，谨防感冒，多喝热水驱寒！\n'
    elif float(result['now']['temp']) < 10.00 and float(result['now']['temp']) > 0.00:
        meg += '\n今天温度偏低，建议穿风衣、厚外套，是时候穿秋裤了！\n'
    elif float(result['now']['temp']) < 0.00:
        meg += '\n今天零下啦！快点穿上棉服或者羽绒服吧！记得穿棉裤棉鞋啊！\n'
    else:
        print('请求接口失败！！')
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'
    }
    r = requests.get('http://open.iciba.com/dsapi/', headers=headers)
    all = json.loads(r.text)
    english = all['content']
    chinese = all['note']
    meg += '\n早安！新的一天元气满满！\n'+english+'\n'+chinese+'\n'
    print(meg)
    return meg


def sendMessage(message, toaddrs):
    # 设置email信息
    # ---------------------------发送字符串的邮件-----------------------------
    # 邮件内容设置
    message = MIMEText(message, 'plain', 'utf-8')
    # 邮件主题
    message['Subject'] = '早安！'
    # 发送方信息
    message['From'] = '<wangshiru2020@126.com>'
    # 接受方信息
    message['To'] = '<1415346329@qq.com>'
    # ---------------------------------------------------------------------
    # 登录并发送邮件
    try:
        # server = smtplib.SMTP('smtp.126.com', 25)
        server = smtplib. SMTP_SSL('smtp.126.com')
        server.connect('smtp.126.com', 465)
        # 163邮箱服务器地址，端口默认为25
        server.login(fromaddr, password)
        server.sendmail(fromaddr, toaddrs, message.as_string())
        print('success')
        server.quit()
    except smtplib.SMTPException as e:
        print('error', e)
        # 打印错误
    return


message = getWeather({'key': '1d20242b74db4349add02547565cb6ff', 'location': '116.23590,40.21808'})
sendMessage(message, ['wangshiru2020@126.com', '2196349054@qq.com'])
message = getWeather({'key': '1d20242b74db4349add02547565cb6ff', 'location': '120.61958,31.29937'})
sendMessage(message, ['wangshiru2020@126.com', '1686778640@qq.com', '15702480741@163.com'])
message = getWeather({'key': '1d20242b74db4349add02547565cb6ff', 'location': '122.99562,41.11062'})
sendMessage(message, ['wangshiru2020@126.com', '1316550730@qq.com'])
